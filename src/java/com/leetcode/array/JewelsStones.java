package com.leetcode.array;

/**
 * 771. 宝石和石头 Jewels and Stones
 *
 * @ClassName JewelsStones
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/10/29 8:56
 * @Version 1.0
 **/
public class JewelsStones {

    public int numJewelsInStones(String J, String S) {

        char[] jewelsChars = J.toCharArray();
        char[] stoneChars = S.toCharArray();

        int count = 0;
        for (int i = 0; i < jewelsChars.length; i++) {
            for (int j = 0; j < stoneChars.length; j++) {
                if (jewelsChars[i] == stoneChars[j]) {
                    count++;
                }
            }
        }
        return count;
    }
}
