package com.leetcode.array;

/**
 * 476. 数字的补数 Number Complement
 * @ClassName NumberComplement
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/8 9:44
 * @Version 1.0
 **/
public class NumberComplement {

    public int findComplement(int num) {

        int i = 0;
        int j = 0;
        while (i < num)
        {
            i += Math.pow(2, j);
            j++;
        }
        return i-num;
    }

    public static void main(String[] args) {
        System.out.println(new NumberComplement().findComplement(2));
    }

}
