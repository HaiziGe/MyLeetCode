package com.leetcode.array;

/**
 * 937 重新排列日志文件 Reorder Log Files
 * @author haizi
 * @date 2018/11/11
 */
public class ReorderLogFiles {

    public String[] reorderLogFiles(String[] logs) {

        String[] zimu = new String[logs.length];
        String[] shuzi = new String[logs.length];
        int zimuIndex = 0;
        int shuziIndex = 0;

        for (String log : logs) {
            if (log.charAt(log.indexOf(" ") + 1) > '9') {
                zimu[zimuIndex++] = log;
            } else {
                shuzi[shuziIndex++] = log;
            }
        }

        for (int i = 0; i < zimuIndex; i++) {
            for (int j = 0; j < i; j++) {
                if (zimu[i].substring(zimu[i].indexOf(" ")+1).compareTo(zimu[j].substring(zimu[j].indexOf(" ")+1)) < 0) {
                    String temp = zimu[i];
                    zimu[i] = zimu[j];
                    zimu[j] = temp;
                }
            }
        }

        // 合并数组

        System.arraycopy(shuzi, 0, zimu, zimuIndex, shuziIndex);

        return zimu;
    }

    public static void main(String[] args) {
        String[] X = {"a1 9 2 3 1","g1 act car","zo4 4 7","ab1 off key dog","a8 act zoo"};
        new ReorderLogFiles().reorderLogFiles(X);
    }
}
