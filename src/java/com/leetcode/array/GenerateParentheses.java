package com.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 22. 生成括号 Generate Parentheses
 * @ClassName GenerateParentheses
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/7 9:46
 * @Version 1.0
 **/
public class GenerateParentheses {

    public List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<>();
        getParenthesis(result, "", 0, 0, n);
        return result;
    }

    private void getParenthesis(List<String> result, String current, int open, int close, int max) {
        if (current.length() == max * 2) {
            result.add(current);
            return;
        }

        if (open < max) {
            getParenthesis(result, current + "(", open+1, close, max);
        }
        if (close < open) {
            getParenthesis(result, current + ")", open, close+1, max);
        }
    }

    public static void main(String[] args) {
        List<String> strings = new GenerateParentheses().generateParenthesis(3);

        for (String string : strings) {
            System.out.println(string);
        }
    }


}
