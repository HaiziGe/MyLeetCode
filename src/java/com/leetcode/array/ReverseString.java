package com.leetcode.array;

/**
 * 344. 反转字符串 Reverse String
 * @author haizi
 * @date 2018/11/10
 */
public class ReverseString {

    public String reverseString(String s) {

        char[] chars = s.toCharArray();
        int length = chars.length;
        char[] result = new char[length];
        for (int i = 0; i < length; i++) {
            result[length - i - 1] = chars[i];
        }

        return new String(result);
    }
}
