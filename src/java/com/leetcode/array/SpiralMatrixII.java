package com.leetcode.array;

/**
 * 59. 螺旋矩阵 II Spiral Matrix II
 * @ClassName SpiralMatrixII
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/2 9:01
 * @Version 1.0
 **/
public class SpiralMatrixII {

    public int[][] generateMatrix(int n) {

        int[][] result = new int[n][n];
        int i = 1;

        // →→→→→→→→→
        // ↑→→→→→→→↓
        // ↑↑→→→→→↓↓
        // ↑←←←←←←↓↓
        // ←←←←←←←←←
        int up=0,down=n-1,left=0,right=n-1;
        while (i <= n*n) {
            for(int j=left;j<=right;j++) {
                result[up][j]=i++;
            }
            up++;
            for(int j=up;j<=down;j++) {
                result[i][right]=i++;
            }
            right--;
            for(int j=right;j>=left;j--) {
                result[down][j]=i++;
            }
            down--;
            for(int j=down;j>=up;j--) {
                result[i][left]=i++;
            }
            left++;
        }

        return result;


    }

}
