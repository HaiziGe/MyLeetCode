package com.leetcode.array;

/**
 * 867. 转置矩阵 Transpose Matrix
 * @ClassName TransposeMatrix
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/13 15:45
 * @Version 1.0
 **/
public class TransposeMatrix {

    public int[][] transpose(int[][] A) {

        int X = A.length;
        int Y = A[0].length;

        int[][] result = new int[Y][X];

        for (int i = 0; i < X; i++) {
            for (int j = 0; j < Y; j++) {
                result[j][i] = A[i][j];
            }
        }
        return result;

    }
}
