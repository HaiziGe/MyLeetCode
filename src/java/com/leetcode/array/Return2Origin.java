package com.leetcode.array;

/**
 * 657. 机器人能否返回原点 Robot Return to Origin
 * @ClassName Return2Origin
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/7 9:02
 * @Version 1.0
 **/
public class Return2Origin {

    public boolean judgeCircle(String moves) {

        int cross = 0;
        int length = 0;

        char[] moveChars = moves.toCharArray();

        for (char moveChar : moveChars) {
            switch (moveChar) {
                case 'U':
                    length++;
                    break;
                case 'D':
                    length--;
                    break;
                case 'L':
                    cross++;
                case 'R':
                    cross--;
            }
        }
        return length == 0 && cross == 0;

    }

    public static void main(String[] args) {
        new Return2Origin().judgeCircle("LL");
    }


}
