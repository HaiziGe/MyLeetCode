package com.leetcode.array;

/**
 * 709. To Lower Case 转换成小写字母
 *
 * @ClassName ToLowerCase
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/10/27 11:35
 * @Version 1.0
 **/
public class ToLowerCase {

    public String toLowerCase(String str) {
        if(null==str) {
            return null;
        }
        char ch[]=str.toCharArray();
        for(int i=0;i<ch.length;i++) {
            if(ch[i]>='A'&&ch[i]<='Z') {
                ch[i]+=32;
            }
        }
        return new String(ch);
    }

}
