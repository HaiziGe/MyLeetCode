package com.leetcode.array;

import java.util.HashMap;
import java.util.Map;

/**
 * 961. 重复 N 次的元素
 * @ClassName RepeatedElement
 * @Description 961. 重复 N 次的元素
 * @Author HaiziGe
 * @Date 2019/1/11 17:19
 * @Version 1.0
 **/
public class RepeatedElement {

    public int repeatedNTimes(int[] A) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            map.put(A[i], map.get(A[i]) != null? map.get(A[i]) + 1 : 1);
            if (map.get(A[i]) ==  A.length/2) {
                return A[i];
            }
        }
        return -1;
    }
}
