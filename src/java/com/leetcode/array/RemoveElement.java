package com.leetcode.array;

/**
 * 27. 移除元素 Remove Element
 * @ClassName RemoveElement
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/15 10:05
 * @Version 1.0
 **/
public class RemoveElement {

    /**
     * 这是一个很愚蠢的做法
     * @param nums
     * @param val
     * @return
     */
    public int removeElement(int[] nums, int val) {

        int count = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                count++;
            } else {
                for (int j = i; j < nums.length; j++) {
                    if (nums[j] != val) {
                        nums[i] = nums[j];
                        nums[j] = val;
                        count++;
                        break;
                    }
                }
            }
        }
        return count;
    }

    /**
     * 正确的做法  类似于长短指针
     * @param nums
     * @param val
     * @return
     */
    public int removeElementCool(int[] nums, int val) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (val != nums[i]){
                nums[index++] = nums[i];
            }
        }
        return index;
    }


}
