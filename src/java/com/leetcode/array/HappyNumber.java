package com.leetcode.array;

/**
 * 202. Happy Number
 * 最终得到的数为1才停止 除了1和7其余的都会继续循环下去
 * @author haizi
 * @date 2018/11/28
 */
public class HappyNumber {

    public boolean isHappy(int num) {
        System.out.print(num+"->");
        if(num < 10) {
            return num == 1 || num == 7;
        }
        int n = num;
        int b = 0;
        while(n > 0) {
            b += (n % 10) * (n % 10);
            n = n / 10;
        }
        return isHappy(b);
    }

    public static void main(String[] args) {
        new HappyNumber().isHappy(7);
    }
}
