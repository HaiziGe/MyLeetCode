package com.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 500. 键盘行
 * @ClassName KeyboardRow
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/12 8:42
 * @Version 1.0
 **/
public class KeyboardRow {

    public String[] findWords(String[] words) {

        String L1 = "qwertyuiopQWERTYUIOP";
        String L2 = "asdfghjklASDFGHJKL";
        String L3 = "zxcvbnmZXCVBNM";

        List<String> list = new ArrayList<>();

        for (int i = 0; i < words.length; i++) {
            if (keyboard(words[i], L1) + keyboard(words[i], L2) + keyboard(words[i], L3) == 1) {
                list.add(words[i]);
            }
        }

        return list.toArray(new String[0]);
    }

    public int keyboard(String s, String L) {

        char[] chars = s.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if (L.indexOf(chars[i]) < 0) {
                return 0;
            }
        }
        return 1;
    }
}
