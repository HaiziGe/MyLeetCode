package com.leetcode.array;

import java.util.HashSet;
import java.util.Set;

/**
 * 929. 独特的电子邮件地址
 * @ClassName UniqueEmailAddresses
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/10/31 8:47
 * @Version 1.0
 **/
public class UniqueEmailAddresses {

    public int numUniqueEmails(String[] emails) {

        Set<String> emailSet = new HashSet<>();

        for (String email : emails) {

            int atIndex = email.indexOf("@");

            String local = email.substring(0, atIndex);
            String rest = email.substring(atIndex);

            local = local.substring(0, email.indexOf("+")).replace(".", "");

            emailSet.add(rest + local);
        }

        return emailSet.size();

    }
}
