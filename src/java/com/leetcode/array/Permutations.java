package com.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 46. 全排列 Permutations
 * @ClassName Permutations
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/12 9:32
 * @Version 1.0
 **/
public class Permutations {

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums.length == 0) {
            return result;
        }
        getPermutations(nums, 0, new ArrayList<>(), result);

        return result;
    }

    private void getPermutations(int[] nums, int start, List<Integer> permutes, List<List<Integer>> result) {
        if (permutes.size() == nums.length) {
            result.add(permutes);
            return;
        }

        for (int i = 0; i <= permutes.size(); i++) {
            List<Integer> temp = new ArrayList<>(permutes);
            temp.add(i, nums[start]);
            getPermutations(nums, start+1, temp, result);
        }
    }

    public static void main(String[] args) {

        int[] x = {1,2,3};

        new Permutations().permute(x);

    }
}
