package com.leetcode.array;

import java.util.*;

/**
 *
 *  807. 保持城市天际线
 *
 * @ClassName CitySkyline
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/10/27 10:50
 * @Version 1.0
 **/
public class CitySkyline {


    public int maxIncreaseKeepingSkyline(int[][] grid) {
        int N = grid.length;

        int[] rowMax = new int[N];
        int[] colMax = new int[N];

        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                rowMax[i] = Math.max(rowMax[i], grid[i][j]);
                colMax[j] = Math.max(colMax[j], grid[i][j]);
            }
        }

        int count = 0;

        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                count += Math.min(rowMax[i], colMax[j]) - grid[i][j];
            }
        }
        return count;

    }

    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer, Integer> amap = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (amap.containsKey(target - nums[i])) {
                result[0] = i;
                result[1] = amap.get(target - nums[i]);
                break;
            }
            amap.put(nums[i], i);
        }
        return result;
    }


    public static void main(String[] args) {
        CitySkyline citySkyline = new CitySkyline();
        int[][] grid = {{59,88,44},{3,18,38},{21,26,51}};
        System.out.println(citySkyline.maxIncreaseKeepingSkyline(grid));
    }
}
