package com.leetcode.array;

/**
 * 136.只出现一次的数字 Single Number
 * @author haizi
 * @date 2018/11/18
 */
public class SingleNumber {

    public int singleNumber(int[] nums) {
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            result ^= nums[i];
        }
        return result;

    }
}
