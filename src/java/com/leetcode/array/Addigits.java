package com.leetcode.array;

/**
 * @author haizi
 * @date 2018/11/20
 */
public class Addigits {

    public int addDigits(int num) {

        while (num > 10) {
            int sum = 0;
            while (num > 0) {
                sum += num % 10;
                num /= 10;
            }
            num = sum;
        }
        return num;

    }

}
