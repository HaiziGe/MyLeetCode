package com.leetcode.array;

/**
 * 461. 汉明距离 Hamming Distance
 *
 * @ClassName HammingDistance
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/6 10:56
 * @Version 1.0
 **/
public class HammingDistance {

    public int hammingDistance(int x, int y) {
        int num = x ^ y;
        int count = 0;

        while (num != 0)
        {
            count++;
            num &= num - 1;
        }

        return count;
    }

}
