package com.leetcode.array;

/**
 *
 * 861. 翻转矩阵后的得分 Score After Flipping Matrix
 *
 * @ClassName FlippingMatrix
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/5 10:24
 * @Version 1.0
 **/
public class FlippingMatrix {

    public int matrixScore(int[][] A) {
        int result = 0;
        int row_length = A.length;
        int col_length = A[0].length;
        for (int i = 0; i < row_length; i++) {
            if (A[i][0] > 0) {
                for (int j = 0; j < col_length; j++) {
                    A[i][j] = 1 - A[i][j];
                }
            }
        }

        for (int i = 0; i < col_length; i++) {
            int count0 = 0;
            int count1 = 0;
            for (int j = 0; j < row_length; j++) {
                if (A[j][i] == 0) {
                    count0++;
                } else {
                    count1++;
                }
            }
            if (count0 > count1) {
                for (int m = 0; m < row_length; m++) {
                    A[m][i] = 1 - A[m][i];
                }
            }
        }

        for (int i = 0; i < row_length; i++) {
            for (int j = 0; j < col_length; j++) {
                result += A[i][j] * Math.pow(2, col_length - j - 1);
            }
        }
        
        return result;

    }

    public static void main(String[] args) {

        int[][] A = {{0,0,1,1},{1,0,1,0},{1,1,0,0}};
        new FlippingMatrix().matrixScore(A);

    }
    
    
    
    
}
