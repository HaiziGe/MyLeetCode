package com.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 728. 自除数 Self Dividing Numbers
 * @author haizi
 * @date 2018/11/11
 */
public class SelfDividingNumbers {

    public List<Integer> selfDividingNumbers(int left, int right) {

        List<Integer> result = new ArrayList<>();

        for (int i = left; i <= right; i++) {
            if (isSelfDividingNumbers(i)) {
                result.add(i);
            }

        }
        return result;
    }

    private boolean isSelfDividingNumbers(int i) {
        int temp = i;
        int tail = 0;
        while (temp > 0) {
            tail = temp % 10;
            if (tail == 0 || i % tail != 0) {
                return false;
            }
            temp /= 10;
        }
        return true;

    }


}
