package com.leetcode.array;

/**
 * 942. 增减字符串匹配
 * @author haizi
 * @date 2019/1/1
 */
public class DIStringMatch {

    public int[] diStringMatch(String S) {

        int[] res = new int[S.length() + 1];
        int I = 0;
        int D = S.length();
        char[] chars = S.toCharArray();

        for (int i = 0; i < S.length(); i++) {

            if (chars[i] == 'I') {
                res[i] = I++;
            } else if (chars[i] == 'D') {
                res[i] = D--;
            }
        }
        res[S.length()] = I;

        return res;




    }

}
