package com.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 54. 螺旋矩阵 SpiralMatrix
 * @ClassName SpiralMatrix
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/2 15:54
 * @Version 1.0
 **/
public class SpiralMatrix {

    public List<Integer> spiralOrder(int[][] matrix) {

        List<Integer> result = new ArrayList<>();

        if (matrix.length == 0) {
            return result;
        }

        int y_length = matrix.length;
        int x_length = matrix[0].length;

        int i = 0;
        int up=0, down=y_length-1, left=0, right=x_length-1;
        while (up <= down && left <= right) {
            for(int j=left;j<=right;j++) {
                result.add(matrix[up][j]);
            }
            up++;
            for(int j=up;j<=down;j++) {
                result.add(matrix[j][right]);
            }
            right--;
            for(int j=right;j>=left;j--) {
                result.add(matrix[down][j]);
            }
            down--;
            for(int j=down;j>=up;j--) {
                result.add(matrix[j][left]);
            }
            left++;
        }

        return result;
    }

    public static void main(String[] args) {
        int[][] x = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        List<Integer> integers = new SpiralMatrix().spiralOrder(x);
        for (Integer integer : integers) {
            System.out.print(integer + ",");
        }
    }


}
