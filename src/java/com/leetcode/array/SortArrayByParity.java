package com.leetcode.array;

/**
 * 905. 按奇偶排序数组 Sort Array By Parity
 * @ClassName SortArrayByParity
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/1 15:57
 * @Version 1.0
 **/
public class SortArrayByParity {

    public int[] sortArrayByParity(int[] A) {

        int length = A.length;

        int[] result = new int[length];
        int i= 0;
        int j = 1;
        int k = 0;
        for (; i < length; i++) {
            if (A[i] % 2 == 1) {
                result[length-j] = A[i];
            } else {
                result[k] = A[i];
            }
            j++;
        }

        return result;

    }


}
