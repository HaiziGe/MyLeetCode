package com.leetcode.array;

/**
 * 171. Excel表列序号 Excel Sheet Column Number
 * @ClassName ExcelSheetColumnNumber
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/13 15:31
 * @Version 1.0
 **/
public class ExcelSheetColumnNumber {

    public int titleToNumber(String s) {
        int result = 0;
        char[] chars = s.toCharArray();

        for (int i = chars.length-1; i > 0 ; i--) {
            result = result + (int)((chars[i]-'A'+1) * Math.pow(26, chars.length-i-1));
        }

        return result;

    }

    public static void main(String[] args) {
        new ExcelSheetColumnNumber().titleToNumber("AB");
    }
}
