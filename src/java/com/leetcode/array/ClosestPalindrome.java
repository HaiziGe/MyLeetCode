package com.leetcode.array;

/**
 * 564. 寻找最近的回文数 Find the Closest Palindrome
 * @ClassName ClosestPalindrome
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/14 9:54
 * @Version 1.0
 **/
public class ClosestPalindrome {

    public String nearestPalindromic(String n) {
        long num = Long.parseLong(n);
        for (long i = 1;; i++) {
            if (isPalindrome((num - i) + ""))
                return "" + (num - i);
            if (isPalindrome((num + i) + ""))
                return "" + (num + i);
        }
    }
    private boolean isPalindrome(String str) {
        char[] chars = str.toCharArray();
        for(int i=0;i<=chars.length/2;i++) {
            if(chars[i] != chars[chars.length-i-1]) {
                return false;
            }
        }
        return true;
    }

//    public String nearestPalindromic(String n) {
//        int length = n.length();
//        int intStart = Integer.parseInt(n.substring(0,1));
//        long intN = Long.parseLong(n);
//        if (length < 2) {
//            return intN-1 + "";
//        }
//        for (long i = intN-1; i > 0; i--) {
//            if (isPalindrome(i+"")) {
//                System.out.println(i);
//                return i + "";
//            }
//            if (isPalindrome(++intN + "")) {
//                System.out.println(intN);
//                return intN + "";
//            }
//        }
//        return "";
//    }
//
//    private boolean isPalindrome(String str) {
//        System.out.println(str);
//        char[] chars = str.toCharArray();
//        for(int i=0;i<=chars.length/2;i++) {
//            if(chars[i] != chars[chars.length-i-1]) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//
//
//    public String mirroring(String s) {
//        String x = s.substring(0, (s.length()) / 2);
//        return x + (s.length() % 2 == 1 ? s.charAt(s.length() / 2) : "") + new StringBuilder(x).reverse().toString();
//    }
//    public String nearestPalindromic(String n) {
//        if (n.equals("1"))
//            return "0";
//
//        String a = mirroring(n);
//        long diff1 = Long.MAX_VALUE;
//        diff1 = Math.abs(Long.parseLong(n) - Long.parseLong(a));
//        if (diff1 == 0)
//            diff1 = Long.MAX_VALUE;
//
//        StringBuilder s = new StringBuilder(n);
//        int i = (s.length() - 1) / 2;
//        while (i >= 0 && s.charAt(i) == '0') {
//            s.replace(i, i + 1, "9");
//            i--;
//        }
//        if (i == 0 && s.charAt(i) == '1') {
//            s.delete(0, 1);
//            int mid = (s.length() - 1) / 2;
//            s.replace(mid, mid + 1, "9");
//        } else
//            s.replace(i, i + 1, "" + (char)(s.charAt(i) - 1));
//        String b = mirroring(s.toString());
//        long diff2 = Math.abs(Long.parseLong(n) - Long.parseLong(b));
//
//
//        s = new StringBuilder(n);
//        i = (s.length() - 1) / 2;
//        while (i >= 0 && s.charAt(i) == '9') {
//            s.replace(i, i + 1, "0");
//            i--;
//        }
//        if (i < 0) {
//            s.insert(0, "1");
//        } else
//            s.replace(i, i + 1, "" + (char)(s.charAt(i) + 1));
//        String c = mirroring(s.toString());
//        long diff3 = Math.abs(Long.parseLong(n) - Long.parseLong(c));
//
//        if (diff2 <= diff1 && diff2 <= diff3)
//            return b;
//        if (diff1 <= diff3 && diff1 <= diff2)
//            return a;
//        else
//            return c;
//    }

    public static void main(String args[]){
        System.out.println(new ClosestPalindrome().nearestPalindromic("88"));
    }

}