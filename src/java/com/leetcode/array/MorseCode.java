package com.leetcode.array;

import java.util.HashSet;
import java.util.Set;

/**
 * 804.  唯一摩尔斯密码词 Unique Morse Code Words
 *
 * @ClassName MorseCode
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/9 13:07
 * @Version 1.0
 **/
public class MorseCode {

    public int uniqueMorseRepresentations(String[] words) {
        String[] morse = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};

        Set<String> morseCodeWords = new HashSet<>();

        for (String word : words) {
            char[] chars = word.toCharArray();
            StringBuilder temp = new StringBuilder();
            for (char aChar : chars) {
                temp.append(morse[aChar - 97]);
            }
            morseCodeWords.add(temp.toString());
        }

        return morseCodeWords.size();

    }

    public static void main(String[] args) {
        String[] X = {"gin", "zen", "gig", "msg"};
        new MorseCode().uniqueMorseRepresentations(X);
    }

}
