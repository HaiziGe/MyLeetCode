package com.leetcode.array;

import java.util.ArrayList;
import java.util.List;

/**
 * 77. 组合 Combinations
 * @ClassName Combinations
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/13 15:59
 * @Version 1.0
 **/
public class Combinations {

    public List<List<Integer>> combine(int n, int k) {
        List<Integer> temp = new ArrayList<>();
        List<List<Integer>> result = new ArrayList<>();

        getCombine(result, temp, n, k, 1);

        return result;

    }

    private void getCombine(List<List<Integer>> result, List<Integer> temp, int n, int k, int index) {
        if (temp.size() >= k) {
            result.add(new ArrayList<>(temp));
            return;
        }
        for (int i = index; i <= n; i++) {
            if (k-temp.size()>n-i+1) {
                return;
            }
            temp.add(i);
            getCombine(result, temp, n, k, i+1);
            temp.remove(temp.size()-1);
        }
    }
}
