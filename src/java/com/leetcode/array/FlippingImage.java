package com.leetcode.array;

/**
 * 832. 翻转图像 Flipping an Image
 * @ClassName FlippingImage
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/5 15:14
 * @Version 1.0
 **/
public class FlippingImage {

    public int[][] flipAndInvertImage(int[][] A) {

        int row_length = A.length;
        int col_length = A[0].length;

        int[][] result = new int[row_length][col_length];

        for (int i = 0; i < row_length; i++) {
            for (int j = 0; j < col_length; j++) {
                result[i][j] = A[i][col_length-j-1] ^ 1;
            }
        }
        return result;

    }



}
