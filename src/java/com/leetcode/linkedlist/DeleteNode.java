package com.leetcode.linkedlist;

/**
 * 237. 删除链表中的节点 Delete Node in a Linked List
 * @ClassName DeleteNode
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/15 9:11
 * @Version 1.0
 **/
public class DeleteNode {
    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}
