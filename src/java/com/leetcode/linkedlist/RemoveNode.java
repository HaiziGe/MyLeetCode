package com.leetcode.linkedlist;

/**
 * 203.  移除链表元素 Remove Linked List Elements
 * @ClassName RemoveNode
 * @Description 我是一个注释
 * @Author HaiziGe
 * @Date 2018/11/15 9:21
 * @Version 1.0
 **/
public class RemoveNode {

    public ListNode removeElements(ListNode head, int val) {
        if (head == null) {
            return head;
        }
        ListNode temp = new ListNode(0);
        temp.next = head;
        while (head.next != null) {
            if (head.next.val == val) {
                head.next = head.next.next;
            } else {
                head = head.next;
            }
        }
        return temp.next;
    }

}
